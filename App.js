/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
require('react-native-highlight-updates');
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TextInput, FlatList, SafeAreaView, ActivityIndicator } from 'react-native';
import { nameService } from './src/services/NameService';
import NameList from './src/shared/components/NameList';

export default class App extends Component {
  state = {
    nameToSearch: '',
    names: [],
    loading: false
  }

  componentDidMount() {
    // // To see all the requests in the chrome Dev tools in the network tab.
    // XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
    //   GLOBAL.originalXMLHttpRequest :
    //   GLOBAL.XMLHttpRequest;

    // // fetch logger
    // global._fetch = fetch;
    // global.fetch = function (uri, options, ...args) {
    //   return global._fetch(uri, options, ...args).then((response) => {
    //     console.log('Fetch', { request: { uri, options, ...args }, response });
    //     return response;
    //   });
    // };
  }

  getNames(name) {
    const { nameToSearch } = this.state;
    this.setState({loading: true}, () => {
      nameService.getNames(name).then(names => {
        this.setState({ names, loading: false })
      }).catch(error => {
        console.log(error);
      });
    });
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>
          Name Search
        </Text>
        <TextInput
          style={styles.inputStyle}
          onChangeText={(text) => {
            this.getNames(text)
          }}
          placeholder="Type a name..."
        />
        <View>
          {
            this.state.loading ? <ActivityIndicator size="large" color="#0000ff" /> : null
          }

          <NameList names={this.state.names} />

          {/* <FlatList
            data={this.state.names}
            renderItem={({ item }) => <Text style={styles.nameStyle}>{item}</Text>}
            keyExtractor={(item, index) => item}
          /> */}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    width: '100%',
    height: '100%',
    flex: 1,
    alignItems: 'center',
  },
  inputStyle: {
    width: 250,
    backgroundColor: 'white',
    padding: 10,
    marginBottom: 20
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    paddingBottom: 20,
  },
  nameStyle: {
    fontSize: 14,
    marginBottom: 10,
  }
});
