import axios, {CancelToken} from 'axios';

class NameService {
    CancelToken;
    cancel;

    getNames(name) {
        const self = this;
        if (self.cancel) {
            // cancel the request
            self.cancel('User typed another thing');
        }
        return new Promise((resolve, reject) => {
            axios.get(`http://localhost:3000/searchUsers?name=${name}`, {
                cancelToken: new CancelToken(function executor(c) {
                  // An executor function receives a cancel function as a parameter
                  self.cancel = c;
                })
              }).then(response => {
                self.cancel = null;
                resolve(response.data);
            }).catch(error => {
                reject(error);
            });
        })
    }
}

export const nameService = new NameService();