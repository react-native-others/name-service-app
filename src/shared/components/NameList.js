import React, { Component, PureComponent } from 'react';
import { Text, View } from 'react-native';

export default class NameList extends PureComponent {
    render() {
        return (
            <View>
                {this.props.names.map((n, i) => <NameContainer key={i} name={n} />)}
            </View>
        )
    }
}

class NameContainer extends PureComponent {
    render() {
        return (
            <Text>{this.props.name}</Text>
        );
    }
}